# Getting Started
- Download and install from: [git-scm](https://git-scm.com/ "Git")
- Check Installation: `git --version`
- Create *SSH Key*:
	1. Open Git Bash
	2. Run `ssh-keygen`
- Add *SSH Key* to GitLab (it may be similar at other hosters):
	1. Navigate to your *Account>Settings>SSH Keys*
	2. Paste your *public* key and save it there

# Commands (essential)
- `git add <FILE>` Adds untracked files, like new or renamed files
- `git clone <SOURCE> <DIRECTORY>` Gets repository from SOURCE and saves it in DIRECTORY
- `git commit -m <MESSAGE>` Records changes with an associated message
- `git config <CONTEXT> --list` Lists all git variables (with or without context, e.g. --local)
- `git config <CONTEXT> <KEY> <VALUE>` Saves KEY and VALUE in CONTEXT
- `git init` Creates *.git* folder in your project to track changes
- `git merge <BRANCH>` Joins branches together
- `git pull -u <DESTINATION> master` Applies changes from destination/origin on the master branch
- `git push -u <DESTINATION> master` Applies changes to destination/origin on the master branch
- `git remote add origin <URL>` Adds an origin, then you don't have to enter the whole URL (e.g. to pull or push a project)
- `git reset --hard HEAD~2` Resets (HEAD~2 = last two commits) with (--hard = commits and files) mode
- `git reset --soft HEAD~2` Resets (HEAD~2 = last two commits) with (--soft = commits) mode
- `git rm <FILE>` Removes files from tracking and deletes them after the next pull/push
- `git status` Shows uncommited or untracked directories/files

# The .gitignore file
Contains directories/files, which should not be tracked (ignored), like e.g. target files. For example:
```
/myIgnoredDirectory
myIgnoredFile
```